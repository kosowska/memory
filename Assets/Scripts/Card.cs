﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Card : MonoBehaviour {
    public GameObject matchedCard;
    public Sprite image;
    public Sprite defaultImage;
    public GameObject game;
    void SetImage(Sprite img)
    {
        GetComponent<Image>().sprite = img;
    }
    public void ShowImageOnClick()
    {
        Game gameComp = game.GetComponent<Game>();
        if (gameComp.clickedCards.Count < 2)
        {
            gameComp.clickTime = Time.time;
            gameComp.clickedCards.Add(this.gameObject);

            SetImage(image);
        }
    }
    public void SetDefaultImage()
    {
        SetImage(defaultImage);
    }
	// Use this for initialization
	void Start () {
        SetImage(defaultImage);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
