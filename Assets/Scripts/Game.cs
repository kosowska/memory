﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class Game : MonoBehaviour {
    public GameObject canvas;
    public GameObject cardPrefab;
    public int cardsCounter;
    int howManyPairs;
    float sndClickTime;
    public float clickTime;
    
    public int cardsForX;
    public int cardsForY;
    List<Vector3> cardsPositions;
    public List<GameObject> clickedCards;
     List<GameObject> cards;
    public GameObject grid;
    bool win;
    //public UnityEngine.UI.Text winText;
    public GameObject winImage;
    public UnityEngine.UI.Text scoreText;
    public List<Sprite> cardImages;
    public GameObject textBackground;
    public AudioClip music;
    public AudioSource source;

    /*void ComputeCardsPositions()
    {
        int width = Screen.width;
        int height = Screen.height;
        int xStep = width / cardsForX;
        int yStep = height / cardsForY;
        
        Vector3 firstCardPos = firstCard.transform.position;
        Vector3 newCardPos = firstCardPos;
        Debug.Log(newCardPos);
        Debug.Log(height);
        Debug.Log(width);
        Debug.Log(yStep);

        for (int i=0; i+yStep < height; i+= yStep)
        {
            for (int j=0;j+xStep< width; j+= xStep)
            {
                newCardPos.x = j+firstCardPos.x;
                newCardPos.y = -i+firstCardPos.y;
                Debug.Log("nowa pozycja");
                Debug.Log(newCardPos);
                cardsPositions.Add(newCardPos);

        }
    }*/
    void SettleCardsOnGrid()
    {
        
        for (int i = 0; i < cards.Count; i++)
        {
            cards[i].transform.SetParent(grid.transform);
        }
    }
    int[] RandomPermutation(int n)
    {
        UnityEngine.Random rnd = new UnityEngine.Random();
        int[] perm = new int[n + 1];
        for (int i = 0; i < n; i++)
        {
            perm[i] = i;
        }
        int change,tmp;
        for(int i=n-1;i>0;i--)
        {
            change = (int)UnityEngine.Random.Range(0f,(float)(i-1));
            Debug.Log("RANDOM");
            Debug.Log(change);
            tmp = perm[change];
            perm[change] = perm[i];
            perm[i] = tmp;
        }
        Debug.Log("PERMUTATION:");
        for(int i=0;i<n;i++)
        {
            Debug.Log(perm[i]);
        }
        return perm;
    }
    void ChangeImages()
    {
        int[] perm = RandomPermutation(cardImages.Count-1);
        for(int i=0;i<howManyPairs*2;i+=2)
        {
            cards[i].GetComponent<Card>().image = cardImages[perm[i]];
            cards[i].GetComponent<Card>().matchedCard.GetComponent<Card>().image = cardImages[perm[i]];
        }

    }
    void ShuffleCards()
    {
        int[] perm = RandomPermutation(cards.Count);
        for(int i=0;i<cards.Count;i++)
        {
            cards[i].transform.SetSiblingIndex(perm[i]);
        }
    }
    void MakeCards()
    {
        //Card c = new Card
        for(int i = 0; i < howManyPairs; i++)
        {
            GameObject c = Instantiate(cardPrefab, cardPrefab.transform.position, cardPrefab.transform.rotation);
            GameObject cMatch = Instantiate(cardPrefab, cardPrefab.transform.position, cardPrefab.transform.rotation);
            c.transform.parent = canvas.transform;
            cMatch.transform.parent = canvas.transform;
            c.GetComponent<Card>().matchedCard = cMatch;
            cMatch.GetComponent<Card>().matchedCard = c;
            c.GetComponent<Card>().game = this.gameObject;
            cMatch.GetComponent<Card>().game = this.gameObject;
            cards.Add(c);
            cards.Add(cMatch);
        }
    }
    void SetCardsPositions()
    {
        ChangeImages();
        Debug.Log("pozycje kart");
        Debug.Log(cardsPositions.Count);
        ShuffleCards();
    }
    void IsWin()
    {
        if(howManyPairs == cardsCounter)
        {
            win = true;
            source.Stop();
            winImage.gameObject.SetActive(true);
            textBackground.SetActive(true);
            Debug.Log("wygrana!");
        }
        
    }
    void TwoClicked()
    {
        if (clickedCards[0].GetComponent<Card>().matchedCard == clickedCards[1])
        {
            Debug.Log("dobrze!");
            clickedCards.Clear();
            cardsCounter++;
            scoreText.text = "Score : " + cardsCounter.ToString();
            IsWin();
        }
        else
        {
            if (Time.time - sndClickTime > 2.0)
            {
                clickedCards[0].GetComponent<Card>().SetDefaultImage();
                clickedCards[1].GetComponent<Card>().SetDefaultImage();
                clickedCards.Clear();
            }
        }
    }

    void Start()
    {
        howManyPairs = cardsForX * cardsForY / 2;
        cards = new List<GameObject>();
        clickedCards = new List<GameObject>();
        cardsPositions = new List<Vector3>();
        MakeCards();
//        winText.transform.SetAsLastSibling();
        scoreText.transform.SetAsLastSibling();
        textBackground.transform.SetAsLastSibling();
        //firstCard.gameObject.SetActive(false);
        win = false;
        cardsCounter = 0;
        winImage.gameObject.SetActive(false);
        textBackground.SetActive(false);
        //ComputeCardsPositions();
        SettleCardsOnGrid();
        SetCardsPositions();
        source = GetComponent<AudioSource>();
        source.loop = true;
        source.clip = music;
        source.Play();



    }
    void Update () {
        if(clickedCards.Count == 2)
            {
                sndClickTime = clickTime;
                TwoClicked();
            }

	}
}
